import React from 'react';
import {
  View,
  Image,
  Alert,
  Button,
  Keyboard,
  StyleSheet,
  KeyboardAvoidingView,
  TouchableWithoutFeedback
} from 'react-native';
import { keys } from 'lodash'
import InputField from './components/InputField'

export default class App extends React.Component {
  constructor(props) {
    super(props);
  
    this.state = {
      errors: {},
      fields: {
        username: '',
        password: ''
      }
    };
  }

  changeHandler(field, value) {
    console.log(`${field}: ${value}`)
    const { 
      fields: prevfields, 
      errors: prevError 
    } = this.state

    const fields = { ...prevfields, [field]: value }    
    const errors = this.validate(fields, prevError, field)

    this.setState({ fields, errors })
  }

  submitHandler() {
    const { 
      fields: prevData, 
      errors: prevError 
    } = this.state

    const errors = this.validate(prevData, prevError)

    this.setState({ errors }, () => {
      if (!keys(errors).filter(key => errors[key]).length)
        Alert.alert('Login is successful!')
    })
  }

  validate(fields, prevError, key) {
    const errors = {}
    keys(fields).map(field => {
      const value = fields[field].trim()

      if (key && field !== key)
        return

      errors[field] = '' // to reset the previous value

      if (!value) // empty
        errors[field] = `${field} must not empty`

      if (value && field.includes('username') && !/\S+@\S+\.\S+/.test(value)) // valid username
        errors[field] = `invalid username`

      if (field.includes('pass') && value && !(value.length > 6 && value.length < 12) ) // valid password
        errors[field] = 'please use at least 8 characters'
    })  

    return { ...prevError, ...errors }
  }

  render() {
    const { errors, fields } = this.state
    const disable = !!keys(errors).filter(key => errors[key]).length

    return (      
      <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>                       
        <View style={styles.container}>
          <Image source={require('./img/logo.png')} style={styles.image_containter}/>
          {
            keys(fields).map((field, index) => (
              <InputField 
                key={`${index}-${field}`}
                unique_key={`${index}-${field}`}
                field={field} 
                fields={fields} 
                errors={errors}
                handleInputChange={this.changeHandler.bind(this)}
              /> 
            ))
          }
          <View style={styles.view}>
            <Button
              title="Sign In"
              accessibilityLabel="Sign In"
              color="#714db2"           
              disabled={disable}
              style={styles.button}
              onPress={this.submitHandler.bind(this)}
            />  
          </View>                     
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  image_containter: {
    alignSelf: 'center',
    marginBottom: 60,
    marginTop: 0,
  },  
  view: {
    padding: 10,
    paddingBottom: 0,
    alignSelf: 'stretch',
  },
  button: {
    borderRadius: 5,
    borderStyle: 'solid',
    borderColor: '#714DB1',
    borderWidth: 1,
    padding: 10,
    fontSize: 18,
    alignSelf: 'stretch',
  }
});
