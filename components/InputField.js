import React from 'react';
import {
  StyleSheet,
  Keyboard, 
  TextInput,
  Text, 
  View,
  } from 'react-native';


function toUpperCase(string) {
  return string.charAt(0).toUpperCase() + string.slice(1)
}

export default function({ unique_key, field, fields, errors, handleInputChange }) {
  return (
    <View style={styles.view} key={unique_key}>
      <Text style={styles.label}>{ toUpperCase(field) }</Text>
      <TextInput
        placeholder={`Input ${field}`}
        style={styles.field}
        underlineColorAndroid='transparent'
        autoFocus
        multiline
        onSubmitEditing={Keyboard.dismiss}
        onChangeText={ text => handleInputChange(field, text) }
        value={fields[field]}
      />
      <Text style={styles.label_error}>{ errors[field] }</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  field: {
    borderRadius: 5,
    borderStyle: 'solid',
    borderColor: '#714DB1',
    borderWidth: 1,
    padding: 10,
    fontSize: 18,
    alignSelf: 'stretch',
  },
  label: {
    textAlign: 'left',
    fontWeight: 'bold',
    fontSize: 20
  },
  view: {
    padding: 10,
    paddingBottom: 0,
    alignSelf: 'stretch',
  },
  label_error: {
    color: '#de2222'
  }
});
